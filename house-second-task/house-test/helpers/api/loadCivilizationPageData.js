// Request template
import request from './request';

/**
 * Функция "loadCivilizationPageData" посылает запрос за данными для страницы цивилизации.
 *
 * @param {number} [civilizationId] - ID цивилизации.
 */
const loadCivilizationPageData = async (civilizationId) => {
  const url = `https://age-of-empires-2-api.herokuapp.com/api/v1/civilization/${civilizationId}`;

  const data = await request({ url });

  return data;
};

export default loadCivilizationPageData;
