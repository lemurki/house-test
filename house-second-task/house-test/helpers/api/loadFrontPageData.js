// Request template
import request from './request';

/**
 * Функция `loadFrontPageData` посылает запрос за содержимым главной страницы.
 */
const loadFrontPageData = async () => {
  const url = 'https://age-of-empires-2-api.herokuapp.com/api/v1/civilizations';
  const data = await request({ url });
  return data;
};

export default loadFrontPageData;
