
/**
 * Функция "request" является оберткой для всех функций-запросов.
 * Выполняет метод fetch c указанным URL и параметрами, и возвращает полученный ответ.
 *
 * @param {object} params
 * @param {string} params.url - URL адрес для запроса.
 * @param {object} [params.options] - Объект с дополнительными опциями для fetch.
 * @param {object} [params.headers] - Объект с параметрами Headers.
 * @param {object} [params.method] - Метод запроса (по умолчанию GET).
 * @param {object} [params.onFailure] - Функция, которая срабатывает в случае ошибки.
 */
const request = async ({
  url,
  options = {},
  headers = {},
  method = 'GET',
  onFailure = undefined,
}) => {
  let data = null;
  let status = null;

  await fetch(url, {
    mode: 'cors',
    method,
    headers,
    ...options,
  })
    .then((res) => {
      status = res.status;
      const isSuccessful = res.status >= 200 && res.status <= 299;

      const contentType = res.headers.get('content-type');
      const isJSONResponse = contentType?.includes('application/json');

      if (isSuccessful) {
        return isJSONResponse ? res.json() : true;
      }

      throw new Error(
        `Ошибка при загрузке данных!
        Адрес: ${url}
        Статус: ${res.status}`
      );
    })
    .then((res) => {
      data = res;
    })
    .catch((err) => {
      console.error(err);
      if (onFailure) {
        onFailure(status);
      }
      throw err;
    });

  return data;
};

export default request;
