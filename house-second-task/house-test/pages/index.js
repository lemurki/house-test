import Head from 'next/head';
// Helpers
import loadFrontPageData from 'helpers/api/loadFrontPageData';
// Components
import FrontPageContent from 'components/FrontPageContent';

function FrontPage({ civilizations }) {
  return (
    <>
      <Head>
        <title>Civilizations</title>
        <meta name='Civilizations page' content='Civilizations list from Age Of Empires II' />
      </Head>

      <main>
        <FrontPageContent civilizations={civilizations} />
      </main>
    </>
  );
}

export async function getServerSideProps() {
  const { civilizations } = await loadFrontPageData();

  return {
    props: { civilizations }
  };
}

export default FrontPage;
