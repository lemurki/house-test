import Head from 'next/head';
// Helpers
import loadCivilizationPageData from 'helpers/api/loadCivilizationPageData';
// Components
import CivilizationPageContent from 'components/CivilizationPageContent';

function civilizationPage({ civilization }) {
  return (
    <>
      <Head>
        <title>{civilization.name}</title>
        <meta
          name={`${civilization.name} page`}
          content={`All information about ${civilization.name}`}
        />
      </Head>

      <main>
        <CivilizationPageContent civilization={civilization} />
      </main>
    </>
  );
}

export async function getServerSideProps({ query }) {
  const civilizationId = query.id;

  const civilization = await loadCivilizationPageData(civilizationId);

  return {
    props: { civilization }
  };
}

export default civilizationPage;
