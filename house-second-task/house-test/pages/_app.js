import '@styles/global.scss';
import '@styles/normalize.scss'

function MyApp({ Component, pageProps }) {
  return (
    <div id='page-root'>
      {/* Содержимое страницы */}
      <Component {...pageProps} />
    </div>
  );
}

export default MyApp;
