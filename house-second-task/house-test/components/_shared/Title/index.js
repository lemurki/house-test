import PropTypes from 'prop-types';

/**
 * Компонент `Title` отображает стандартный заголовок.
 *
 * @property {string} titleLevel - Уровень заголовка
 * @property {*} children - Содержимое заголовка
 */
function Title({ titleLevel, children, ...props }) {
  switch (titleLevel.toString()) {
    case '1':
      return <h1 {...props}>{children}</h1>;

    case '2':
      return <h2 {...props}>{children}</h2>;

    case '3':
      return <h3 {...props}>{children}</h3>;

    case '4':
      return <h4 {...props}>{children}</h4>;

    case '5':
      return <h5 {...props}>{children}</h5>;

    case '6':
      return <h6 {...props}>{children}</h6>;

    default:
      return null;
  }
}

Title.defaultProps = {
  titleLevel: 2
};

Title.propTypes = {
  titleLevel: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: PropTypes.node.isRequired
};

export default Title;
