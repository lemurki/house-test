import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
// styles
import styles from './styles.module.scss';

/**
 * Компонент `Pagination` служит для отображения пагинации на страницах.
 *
 * @property {number} numberOfPages - Суммарное количество страниц
 * @property {number} currentPage - Номер текущей страницы
 * @property {func} changePage - Функция, запускающая переключение страниц
 * @property {string} [label] - Название навигации.
 */
function Pagination({
  numberOfPages,
  currentPage,
  changePage,
  label
}) {
  return (
    <nav aria-label={label} className={styles.paginate}>
      <ReactPaginate
        previousLabel=''
        nextLabel=''
        breakLabel='...'
        pageCount={numberOfPages}
        marginPagesDisplayed={1}
        pageRangeDisplayed={2}
        forcePage={currentPage - 1}
        onPageChange={(args) => changePage(args.selected + 1)}
        hrefBuilder={(page) => `#${page}`}
        ariaLabelBuilder={(page) => `Страница ${page}`}
        previousLinkClassName='paginationPreviousLink'
        nextLinkClassName='paginationNextLink'
        disableInitialCallback
      />
    </nav>
  );
}

Pagination.defaultProps = {
  label: null
};

Pagination.propTypes = {
  label: PropTypes.string,
  numberOfPages: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  changePage: PropTypes.func.isRequired,
};

export default Pagination;
