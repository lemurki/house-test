import { useReducer } from 'react';
import PropTypes from 'prop-types';
// styles
import styles from './styles.module.scss';
// Components
import CivilizationsListItem from './CivilizationsListItem';
import Pagination from './Pagination';
import Title from '@shared/Title';
// Reducer
import { actionTypes, initialState, reducer } from './reducer';

/**
 * Компонент `FrontPageContent` отображает содержимое главной страницы.
 *
 * @property {array<object>} civilizations - Массив цивилизаций.
 */
function FrontPageContent({ civilizations }) {
  const pageLimit = 10;
  const pagesCount = Math.ceil(civilizations.length / pageLimit);
  // Reducer состояния
  const [state, dispatch] = useReducer(
    reducer,
    initialState,
    (reducerState) => ({
      ...reducerState,
      civilizations: civilizations.slice(0, pageLimit),
      civilizationsNumPages: pagesCount
    })
  );

  /**
   * Функция загружает список цивилизаций, начиная с указанной страницы.
   *
   * @param {number} page - Номер страницы списка цивилизаций.
   */

  const loadCivilizationsByPage = (page) => {
    dispatch({ type: actionTypes.LOAD_CIVILIZATION_BY_PAGE, page });

    const offset = (page - 1) * pageLimit;
    const currentCountries = civilizations.slice(offset, offset + pageLimit);

    dispatch({
      type: actionTypes.LOAD_CIVILIZATION_BY_PAGE_SUCCESS,
      civilizations: currentCountries
    });
  };

  return (
    <>
      <Title titleLevel={1} className={styles.pageTitle}>
        Главная страница
      </Title>
      <section className={styles.section}>
        <Title titleLevel={2}>Список цивилизаций</Title>
        <ul className={styles.list} aria-label='Список цивилизаций'>
          {state.civilizations.map((item) => (
            <CivilizationsListItem
              key={item.id}
              id={item.id}
              name={item.name}
            />
          ))}
        </ul>
      </section>
      <Pagination
        page={state.page}
        civilizationsNumPages={state.civilizationsNumPages}
        loadCivilizationsByPage={loadCivilizationsByPage}
      />
    </>
  );
}

FrontPageContent.propTypes = {
  civilizations: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default FrontPageContent;
