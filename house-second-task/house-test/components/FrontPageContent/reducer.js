const actionTypes = {
  LOAD_CIVILIZATION_BY_PAGE: 'LOAD_CIVILIZATION_BY_PAGE',
  LOAD_CIVILIZATION_BY_PAGE_SUCCESS: 'LOAD_CIVILIZATION_BY_PAGE_SUCCESS'
};

const initialState = {
  civilizations: [],
  page: 1,
  civilizationsNumPages: 1
};

function reducer(state, action) {
  switch (action.type) {
    case actionTypes.LOAD_CIVILIZATION_BY_PAGE:
      return {
        ...state,
        page: action.page
      };
    case actionTypes.LOAD_CIVILIZATION_BY_PAGE_SUCCESS:
      return {
        ...state,
        civilizations: action.civilizations
      };

    default:
      throw new Error('Не поддерживаемый тип action!');
  }
}

export { actionTypes, initialState, reducer };
