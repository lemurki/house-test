import PropTypes from 'prop-types';
// Components
import Pagination from '@shared/Pagination';

/**
 * Компонент `CivilizationPagePagination` отображает блок с пагинацией по списку цивилизаций.
 *
 * @property {number} page - Номер страницы.
 * @property {number} civilizationsNumPages - Количество страниц.
 * @property {func} loadCivilizationsByPage - Функция изменения текущего списка цивилизаций.
 */
function CivilizationPagePagination({
  page,
  civilizationsNumPages,
  loadCivilizationsByPage
}) {
  /**
   * Функция, запускающая загрузку страницы списка цивилизаций.
   *
   * @param {number} selectedPage - Номер страницы для загрузки списка цивилизаций.
   */
  const changePage = (selectedPage) => loadCivilizationsByPage(selectedPage);

  return (
    <Pagination
      numberOfPages={civilizationsNumPages}
      currentPage={page}
      changePage={changePage}
      label='Навигация по страницам списка цивилизаций'
    />
  );
}

CivilizationPagePagination.propTypes = {
  page: PropTypes.number.isRequired,
  civilizationsNumPages: PropTypes.number.isRequired,
  loadCivilizationsByPage: PropTypes.func.isRequired
};

export default CivilizationPagePagination;
