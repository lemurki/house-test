import PropTypes from 'prop-types';
import Link from 'next/link';
// styles
import styles from './styles.module.scss';

/**
 * Компонент `CivilizationsListItem` отображает список цивилизаций.
 *
 * @property {string} name - Названии цивилизации.
 * @property {number} id - Id цивилизации.
 */
function CivilizationsListItem({ name, id }) {
  return (
    <li className={styles.item}>
      <Link href={`/civilization/${id}`}>
        <a href={`/civilization/${id}`}>{name}</a>
      </Link>
    </li>
  );
}

CivilizationsListItem.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired
};

export default CivilizationsListItem;
