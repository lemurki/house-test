import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
// styles
import styles from './styles.module.scss';
// Components
import Title from '@shared/Title';
import CivilizationInfoSection from './CivilizationInfoSection';

/**
 * Компонент `CivilizationPageContent` отображает содержимое страницы цивилизации.
 *
 * @property {object} civilization - Иноформация о цивилизации.
 */
function CivilizationPageContent({ civilization }) {
  const router = useRouter();

  return (
    <>
      <Title className={styles.title} titleLevel={1}>
        {civilization.name}
      </Title>
      <CivilizationInfoSection
        expansion={civilization.expansion}
        army_type={civilization.army_type}
        unique_unit={civilization.unique_unit}
        unique_tech={civilization.unique_tech}
        team_bonus={civilization.team_bonus}
        civilization_bonus={civilization.civilization_bonus}
      />
      <button
        className={styles.button}
        type='button'
        onClick={() => router.back()}
      >
        Вернуться назад
      </button>
    </>
  );
}

CivilizationPageContent.propTypes = {
  civilization: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    expansion: PropTypes.string,
    army_type: PropTypes.string,
    unique_unit: PropTypes.arrayOf(PropTypes.string),
    unique_tech: PropTypes.arrayOf(PropTypes.string),
    team_bonus: PropTypes.string,
    civilization_bonus: PropTypes.arrayOf(PropTypes.string)
  }).isRequired
};

export default CivilizationPageContent;
