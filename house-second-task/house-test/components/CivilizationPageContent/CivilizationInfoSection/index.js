import PropTypes from 'prop-types';
// styles
import styles from './styles.module.scss';
// Components
import Title from '@shared/Title';

/**
 * Компонент `CivilizationInfoSection` отображает секциию с инофрмацией о цивилизации.
 *
 * @property {string} [expansion] - Название дополнения.
 * @property {string} [army_type] - Тип войск.
 * @property {array} [unique_unit] - Уникальные юнит.
 * @property {array} [unique_tech] - Уникальная техника.
 * @property {string} [team_bonus] - Командный бонус.
 * @property {array} [civilization_bonus] - Бонусы цивилизации.
 */
function CivilizationInfoSection({
  expansion,
  army_type,
  unique_unit,
  unique_tech,
  team_bonus,
  civilization_bonus
}) {
  return (
    <section className={styles.section}>
      <Title titleLevel={2} className={styles.sectionTitle}>
        Информация о цивилизации
      </Title>
      <ul className={styles.list} aria-label='Список характеристик'>
        {expansion && (
          <li className={styles.item}>
            <Title titleLevel={3}>Дополнение:</Title>
            <span>{expansion}</span>
          </li>
        )}
        {army_type && (
          <li className={styles.item}>
            <Title titleLevel={3}>Тип войск:</Title>
            <span>{army_type}</span>
          </li>
        )}
        {unique_unit.length > 0 && (
          <li className={styles.item}>
            <Title titleLevel={3}>Уникальные юниты:</Title>
            <ol aria-label='Список уникальных унитов'>
              {unique_unit.map((item) => (
                <li key={item}>
                  <a href={item} target='_blank' rel='noopener noreferrer'>
                    {item}
                  </a>
                </li>
              ))}
            </ol>
          </li>
        )}
        {unique_tech.length > 0 && (
          <li className={styles.item}>
            <Title titleLevel={3}>Уникальная техника:</Title>
            <ol aria-label='Список уникальной техники'>
              {unique_tech.map((item) => (
                <li key={item}>
                  <a href={item} target='_blank' rel='noopener noreferrer'>
                    {item}
                  </a>
                </li>
              ))}
            </ol>
          </li>
        )}
        {team_bonus && (
          <li className={styles.item}>
            <Title titleLevel={3}>Командный бонус:</Title>
            <span>{team_bonus}</span>
          </li>
        )}
        {civilization_bonus.length > 0 && (
          <li className={styles.item}>
            <Title titleLevel={3}>Бонусы цивилизации:</Title>
            <ol aria-label='Список бонусов цивилизации'>
              {civilization_bonus.map((item) => (
                <li key={item}>
                  <span>{item}</span>
                </li>
              ))}
            </ol>
          </li>
        )}
      </ul>
    </section>
  );
}

CivilizationInfoSection.defaultProps = {
  expansion: PropTypes.string,
  army_type: PropTypes.string,
  unique_unit: PropTypes.arrayOf(PropTypes.string),
  unique_tech: PropTypes.arrayOf(PropTypes.string),
  team_bonus: PropTypes.string,
  civilization_bonus: PropTypes.arrayOf(PropTypes.string)
};

CivilizationInfoSection.propTypes = {
  expansion: PropTypes.string,
  army_type: PropTypes.string,
  unique_unit: PropTypes.arrayOf(PropTypes.string),
  unique_tech: PropTypes.arrayOf(PropTypes.string),
  team_bonus: PropTypes.string,
  civilization_bonus: PropTypes.arrayOf(PropTypes.string)
};

export default CivilizationInfoSection;
