import iMask from 'imask';

export default function () {
  init();

  function init() {
    let phoneInputs = document.querySelectorAll('input[type="tel"]');

    phoneInputs.forEach((input) => {
      new iMask(input, {
        mask: [
          {
            mask: '+7 (000) 000-00-00',
            startsWith: ''
          }
        ],
        dispatch: function (appended, dynamicMasked) {
          let number = (dynamicMasked.value + appended).replace(/\D/g, '');

          return dynamicMasked.compiledMasks.find(function (m) {
            return number.indexOf(m.startsWith) === 0;
          });
        }
      });
    });
  }
}
