import phoneInputsMask from './phoneInputsMask'
import sendFeedbackForm from './sendFeedbackForm'

document.addEventListener('DOMContentLoaded', function () {
 
  // Маски на телефонные поля
  phoneInputsMask();

  // Подключаем форму отправки обратной связи
  sendFeedbackForm()
});
