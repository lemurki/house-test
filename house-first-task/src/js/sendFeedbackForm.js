import resetForm from './helpers/resetForm';

function sendFeedbackForm() {
  const feedbackForm = document.querySelector('.feedback-section__form');
  const feedbackFormInputs = feedbackForm.querySelectorAll('.form__input');
  const feedbackFormButton = document.querySelector(
    '.feedback-section__form-button'
  );

  if (!feedbackForm) return;

  /**
   * Обработчик отправки формы
   */
  feedbackForm.addEventListener('submit', function (e) {
    e.preventDefault();
    // Формируем массив с данными
    const formFieldsData = [];

    feedbackFormInputs.forEach((item) => {
      formFieldsData.push({ name: item.name, value: item.value });
    });

    const formData = JSON.stringify(formFieldsData);

    // Выключаем кнопку submit
    feedbackFormButton.setAttribute('disabled', 'disabled');
    feedbackFormButton.textContent = 'Отправка...';

    // Отправляем запрос
    sendRequest(formData).then((res) => {
      if (res) {
        // Сбрасываем состояние формы в изначальное
        resetForm(feedbackForm);
      } else {
        feedbackFormButton.textContent = 'Ошибка!';
      }
    });
  });

  async function sendRequest(data) {
    try {
      const res = await fetch('https://httpbin.org/post', {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json;charset=utf-8'
        }),
        body: data
      });
      const resData = await res.json();

      if (resData) {
        // Меняем состояние кнопки submit
        feedbackFormButton.removeAttribute('disabled');
        feedbackFormButton.textContent = 'Отправить';
        alert('Сообщение успешно отправленно');
      }
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  }
}

export default sendFeedbackForm;
