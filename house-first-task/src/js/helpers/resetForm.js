// Функция сбрасывает все заполненные поля формы
function resetApplicationForm(form) {
  // Все поля
  form.reset();
}

export default resetApplicationForm;
