var gulp = require("gulp");
var postcss = require("gulp-postcss");
var buffer = require("vinyl-buffer");
var source = require("vinyl-source-stream");
// `PostCSS` plugins
var postcssPresetEnv = require("postcss-preset-env");
var sourcemaps = require("gulp-sourcemaps");
var postcssimport = require("postcss-import");
var postcssNesting = require("postcss-nesting");
var autoprefixer = require("autoprefixer");
var cssnano = require("cssnano");
// js
var browserify = require("browserify");
var uglify = require("gulp-uglify");
// gzip
var gzip = require("gulp-gzip");

/**
 * CSS task
 */

var plugins = [
  // Concat all into one file
  postcssimport(),
  // CSS nesting
  postcssNesting(),
  // Transform future css syntax
  postcssPresetEnv({
    stage: 2,
  }),
  // Add prefixes
  autoprefixer(),
  // Minify css
  cssnano({
    preset: "default",
  }),
];

gulp.task("css", function () {
  return gulp
    .src("./src/css/styles.css")
    .pipe(sourcemaps.init())
    .pipe(postcss(plugins))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./dist/css/"))
    .pipe(gzip({ append: true }))
    .pipe(gulp.dest("./dist/css/"));
});

/**
 * JS task
 */

gulp.task("js", function () {
  return browserify("src/js/index.js")
    .transform("babelify", { presets: ["env"], plugins: ["transform-runtime"] }) // transform modules
    .bundle() // bundling
    .pipe(source("bundle.js"))
    .pipe(buffer()) // prepare to minifying
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify()) // minify
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./dist/js/"))
    .pipe(gzip({ append: true }))
    .pipe(gulp.dest("./dist/js/"));
});

// Task to build css and js
gulp.task("b", gulp.parallel("css", "js"));

// Task to watch css and js
gulp.task("w", function () {
  gulp.watch(["./src/css/**/*.css"], gulp.parallel("css"));
  gulp.watch(["./src/js/**/*.js"], gulp.parallel("js"));
});
